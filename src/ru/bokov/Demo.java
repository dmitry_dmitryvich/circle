package ru.bokov;

/**
 * Класс, демострирующий возможности класса Circle
 *
 * @author Боков Дмитрий
 */

public class Demo {
    public static void main (String[] argc){

        Circle A = new Circle ();
        Circle B = new Circle ();
        System.out.println("Введите данные для первой окружности: ");
        A.input();
        System.out.println("Введите данные для второй окружности: ");
        B.input();
        A.infoCircle();
        B.infoCircle();
        System.out.println("Площадь окружности " + A.getName() + " = " + A.area());
        System.out.println("Площадь окружности " + B.getName() + " = " + B.area());
        System.out.println("Длина круга " + A.getName() + " = " + A.length());
        System.out.println("Длина круга " + B.getName() + " = " + B.length());
        A.areaVsArea(B);
        System.out.println("Расстояние между центрами окружностей = " + A.howLong(B));
        A.contact(B);
        System.out.println("Окружности были передвинуты:");
        A.randomPoints();
        A.infoCircle();
        B.randomPoints();
        B.infoCircle();
    }
}
