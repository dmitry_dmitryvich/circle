package ru.bokov;

import java.lang.Math;
import java.lang.String;
import java.util.Scanner;

/**
 * Класс для представления окружности
 * @author Боков Дмитрий
 */

public class Circle {
    private double x;
    private double y;
    private double r;
    private String name;

    /**
     * Метод позволяет ввести первоначальные данные(координаты и имя центра окружности, радиус окружности) с помощью клавиатуры
     */
    public void input(){
        Scanner reader = new Scanner(System.in);
        System.out.print("Введите координату X: ");
        x = reader.nextInt();
        System.out.print("Введите координату Y: ");
        y = reader.nextInt();
        System.out.print("Введите радиус окружности: ");
        r = reader.nextInt();
        System.out.print("Введите имя центра окружности: ");
        name = reader.next();
    }

    public Circle(double x, double y, double r, String name){
        this.x=x;
        this.y=y;
        this.r=r;
        this.name=name;
    }

    public Circle(){
        this.x=0.0;
        this.y=0.0;
        this.r=1.0;
        this.name=" ";
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getR() {
        return r;
    }

    public void setR(double r) {
        this.r = r;
    }

    public String getName(){
        return name;
    }

    public void setName(){
        this.name=name;
    }

    /**
     * Метод выводит данные об окружности
     */
    public void infoCircle() {
        System.out.println("Окружность " + name + " с центром (" + x + ";" + y + ") и радиусом " + r);
    }

    /**
     * Метод рассчитывает площадь круга
     */
    public double area (){
        double area = Math.PI*Math.pow(r,2);
        return area;
    }

    /**
     * Метод рассчитывает длину окружности
     */
    public double length(){
        double length = 2 * Math.PI * r;
        return length;
    }

    /**
     * Метод сравнивает площадь двух кругов
     */
    public void areaVsArea(Circle b){
        if(this.area() == b.area()){
            System.out.println("Круги " + this.getName() + " и " + b.getName() + " имеют равную площадь ");
        }
        else {
            System.out.println("Круги " + this.getName() + " и " + b.getName() + " имеют различную площадь");
        }
    }

    /**
     * Метод находится расстояние между центрами окружностей и определяет касаются ли они
     */
    public double howLong(Circle b) {
        double dX = (this.x - b.getX()) * (this.x - b.getX());
        double dY = (this.y - b.getY()) * (this.y - b.getY());
        double touch = Math.sqrt(dX + dY);
        return touch;
    }

    /**
     * Метод, определяющий, касаются ли окружности.
     */
    public void contact(Circle b){
        double d = howLong(b);
        if ((d==this.r+b.r)||(d==Math.abs(this.r-b.r))){
            System.out.println("Окружности " + this.getName() + " и " + b.getName() + " касаются");
        }
        else{
            System.out.println("Окружности " + this.getName() + " и " + b.getName() + " не касаются");
        }
    }

    /**
     * Метод перемещает центр окружности в случайную точку из диапазона ([-99;99],[-99;99])
     */
    public void randomPoints(){
        this.x=-99 + (int)(Math.random() * ((198)+1));
        this.y=-99 + (int)(Math.random() * ((198)+1));
    }
}